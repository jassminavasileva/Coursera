import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    static int minCredit;
    static LocalDate startDate;
    static LocalDate endDate;
    static String path;
    private static Repository repository = new Repository();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the required minimum credit: ");
        minCredit = Integer.parseInt(scanner.nextLine());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        System.out.println("Enter start date \"dd.MM.yyyy\": ");
        startDate = LocalDate.parse(scanner.nextLine(), dtf);
        System.out.println("Enter end date - dd.MM.yyyy: ");
        endDate = LocalDate.parse(scanner.nextLine(), dtf);
//        System.out.println("Enter the path to dir: ");
//        path = scanner.nextLine();

        List<Student> students = repository.getStudents().stream()
                        .filter(s -> s.getTotalCredit() >= minCredit).collect(Collectors.toList());

        System.out.println("Student   Total credit");
        System.out.println("          Course Name   Time   Credit   Instructor");

        for (Student student: students) {
            System.out.println(String.format("%s %d", student.getFirstName() + student.getLastName(), student.getTotalCredit()));

            List<Course> courses = repository.getCompletedCoursesByStudentInDateRange(student.getPin(), startDate, endDate);

            for (Course course: courses) {
                System.out.println(course);
            }
        }
    }

}
