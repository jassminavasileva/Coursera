import java.time.LocalDateTime;

public class Course {

    private int id;

    private String name;

    private int instructorId;

    private String instructorName;

    private int totalTime;

    private int credit;

    private LocalDateTime timeCreated;

    public Course() {
    }

    public Course(int id,
                  String name,
                  int instructorId,
                  int totalTime,
                  int credit,
                  LocalDateTime timeCreated) {
        this.id = id;
        this.name = name;
        this.instructorId = instructorId;
        this.totalTime = totalTime;
        this.credit = credit;
        this.timeCreated = timeCreated;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        return String.format("%s %d %d %s", name, totalTime, credit, instructorName);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInstructorId() {
        return instructorId;
    }

    public void setInstructorId(int instructorId) {
        this.instructorId = instructorId;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }
}
