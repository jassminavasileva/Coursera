import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

class Repository{
    private final String dbUrl, dbUsername, dbPassword;

    Repository() {
        dbUsername = "root";
        dbPassword = "root";
        dbUrl = "jdbc:mysql://localhost:3306/coursera";
    }

    public List<Student> getStudents(){
        String query =
                " select pin, first_name, last_name " +
                        " from students ";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Student> students =  getStudents(resultSet);
                if (students.size() == 0) {
                    System.out.println("no entities found");;
                }
                return students;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getTotalCreditOfStudent(String studentPin){
        int totalCount = 0;
        String query =
                " select sum(credit) as total " +
                        " from courses " +
                        " join students_courses_xref on courses.id = students_courses_xref.course_id " +
                        " where student_pin = ? and completion_date is not null ";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, studentPin);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            totalCount = resultSet.getInt("total");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return totalCount;
    }

    public LocalDate getCompletionDateOfCourseByStudent(String studentPin, int courseId) {
        LocalDate date = null;
        String query =
                " select completion_date " +
                        " from students_courses_xref " +
                        " where student_pin = ? and course_id = ? ";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, studentPin);
            statement.setInt(2, courseId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            date = resultSet.getDate("completion_date").toLocalDate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return date;
    }

    public List<Course> getCoursesByStudent(String studentPin) {
        String query =
                " select name, total_time, credit, instructor_id " +
                " from courses " +
                " join students_courses_xref on courses.id = students_courses_xref.course_id " +
                " where student_pin = ?";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, studentPin);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Course> courses =  getCourses(resultSet);
                if (courses.size() == 0) {
                    System.out.println("no entities found");;
                }
                return courses;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Course> getCompletedCoursesByStudent(String studentPin) {
        String query =
                " select name, total_time, credit, instructor_id " +
                        " from courses " +
                        " join students_courses_xref on courses.id = students_courses_xref.course_id " +
                        " where student_pin = ? and completion_date is not null ";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, studentPin);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Course> courses =  getCourses(resultSet);
                if (courses.size() == 0) {
                    System.out.println("no entities found");;
                }
                return courses;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Course> getCompletedCoursesByStudentInDateRange(String studentPin, LocalDate startDate, LocalDate endDate) {
        String query =
                " select name, total_time, credit, instructor_id, completion_date " +
                        " from courses " +
                        " join students_courses_xref on courses.id = students_courses_xref.course_id " +
                        " where student_pin = ? and completion_date is not null and completion_date >= ? and completion_date <= ? ";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            statement.setString(1, studentPin);
            statement.setString(2, startDate.format(dtf).toString());
            statement.setString(3, endDate.format(dtf).toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Course> courses =  getCourses(resultSet);
                if (courses.size() == 0) {
                    System.out.println("no entities found");;
                }
                return courses;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public Instructor getInstructorById(int instructorId) {
        String query = "select id, first_name, last_name " +
                "from instructors " +
                "where id = ?";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setInt(1, instructorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Instructor> result = getInstructors(resultSet);
                if (result.size() == 0) {
                    System.out.println("Entity not found.");;
                }
                return result.get(0);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Student getStudentByPin(String pin) {
        String query = "select pin, first_name, last_name " +
                "from students " +
                "where pin= ?";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, pin);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Student> result = getStudents(resultSet);
                if (result.size() == 0) {
                    System.out.println("Entity not found.");;
                }
                return result.get(0);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Course> getCourses(ResultSet courseData) throws SQLException {
        List<Course> courses = new ArrayList<>();
        while (courseData.next()) {
            Course course = new Course();
            course.setName(courseData.getString("name"));
            course.setTotalTime(courseData.getInt("total_time"));
            course.setCredit(courseData.getInt("credit"));
            course.setInstructorId(courseData.getInt("instructor_id"));
            course.setInstructorName(String.format("%s %s",
                    getInstructorById(course.getInstructorId()).getFirstName(),
                    getInstructorById(course.getInstructorId()).getLastName()));
            courses.add(course);
        }
        return courses;
    }

    private List<Instructor> getInstructors(ResultSet instructorData) throws SQLException {
        List<Instructor> instructors = new ArrayList<>();
        while (instructorData.next()) {
            Instructor instructor = new Instructor();
            instructor.setId(instructorData.getInt("id"));
            instructor.setFirstName(instructorData.getString("first_name"));
            instructor.setLastName(instructorData.getString("last_name"));
            instructors.add(instructor);
        }
        return instructors;
    }

    private List<Student> getStudents(ResultSet studentData) throws SQLException {
        List<Student> students = new ArrayList<>();
        while (studentData.next()) {
            Student student = new Student();
            student.setCourses(getCoursesByStudent(studentData.getString("pin")));
            student.setTotalCredit(getTotalCreditOfStudent(studentData.getString("pin")));
            student.setPin(studentData.getString("pin"));
            student.setFirstName(studentData.getString("first_name"));
            student.setLastName(studentData.getString("last_name"));
            students.add(student);
        }
        return students;
    }

}
